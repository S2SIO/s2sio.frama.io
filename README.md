% Accueil SIO (2ème saison)
% par [moulinux](https://moulinux.frama.io/)
% (révision 04.09.2023)

# Introduction
* [Compétences][competences] en développement d'applications
* [Logiciels][logiciels] à utiliser
* [Organisations locales][organisations] (accès privé)

# CEDjs
* [CEDjs_1-afficher]

# Documentation technique
* [CEDy] (version de la calculette éco-déplacement avec Tkinter)
* [KEDy] (version de la calculette éco-déplacement avec Kivy)


[organisations]: https://s2sio.frama.io/organisations
[competences]: https://s2sio.frama.io/competences.html
[logiciels]: https://s2sio.frama.io/logiciels.html
[CEDjs_1-afficher]: https://ced.frama.io/CEDjs/1-afficher
[CEDy]: http://ced.frama.io/CEDy/7-GUI-Tkinter
[KEDy]: http://ced.frama.io/KEDy
