% Logiciels
% par [moulinux](https://moulinux.frama.io/)
% (révision 29.09.2020)

> Synthèse sur les logiciels à utiliser
(la liste n'est bien sûr pas exhaustive).

# Bureautique
* La suite logicielle [LibreOffice]

# Web
* Le navigateur Web [Firefox]
* Si vous souhaitez absolument utiliser Chrome, préférez-y sa base *open source*
[Chromium] ou mieux encore [Iridium] (un *fork* de chromium qui respecte votre vie privée)
> Derrières les nombreuses déclinaisons du navigateur *open source* chromium
se cachent souvent des projets qui visent à récupérer vos données.

# Éditeurs / IDE
* [Geany] (un éditeur puissant et léger)
* Si vous installez l'application VScode à partir du fichier binaire proposé
sur le site de l'éditeur Microsoft, vous utiliserez une version de l'éditeur
agrémentée de pisteurs. Pour utiliser une version sans pisteur, 100 % *open source*,
tournez-vous vers [VScodium]


[LibreOffice]: https://libreoffice.org/
[Firefox]: https://www.mozilla.org/fr/firefox/
[Chromium]: https://www.chromium.org/Home
[Iridium]: https://iridiumbrowser.de/
[Geany]: https://geany.org/
[VScodium]: https://vscodium.com/
