% Les compétences pour les développeurs/développeuses en S2SIO
% par [moulinux](https://moulinux.frama.io/)
% (révision 16.04.2022)

Vous trouverez ci-dessous un extrait du [référentiel] SIO (révisé) orienté vers les compétences en développement d'applications.

# Bloc de compétences n°1 ‐ Support et mise à disposition de services informatiques (E4)

## B1.1 : Gérer le patrimoine informatique
+-----------------------------------------------------+-----------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                  |
+=====================================================+=============================================================================+
| #. Recenser et identifier les ressources numériques | - Le recensement du patrimoine informatique est exhaustif et réalisé au     |
| #. Exploiter des référentiels, normes et standards  | moyen d’un outil de gestion des actifs informatiques.                       |
| adoptés par le prestataire informatique             | - Les référentiels, normes et standards sont mobilisés de façon pertinente. |
| #. Mettre en place et vérifier les niveaux          | - Les droits mis en place correspondent aux habilitations des acteurs.      |
| d’habilitation associés à un service                | - Les conditions de continuité et de reprise d’un service sont vérifiées et |
| #. Vérifier les conditions de la continuité d’un    | les manquements sont signalés.                                              |
| service informatique                                | - Les sauvegardes sont réalisées dans les conditions prévues conformément   |
| #. Gérer des sauvegardes                            | au plan de sauvegarde.                                                      |
| #. Vérifier le respect des règles d’utilisation     | - Les restaurations sont testées et opérationnelles.                        |
| des ressources numériques                           | - Les écarts par rapport aux règles d’utilisation des ressources numériques |
|                                                     | sont détectés et signalés.                                                  |
+---+---+

## B1.2 : Répondre aux incidents et aux demandes d’assistance et d’évolution
+-----------------------------------------------------+-----------------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                        |
+=====================================================+===================================================================================+
| #. Collecter, suivre et orienter des demandes       | - En utilisant les outils adaptés, les demandes d’assistance ont été prises en    | 
| #. Traiter des demandes concernant les services     | compte, correctement diagnostiquées et leur traitement correspond aux attentes.   |
| réseau et système, applicatifs                      | - La réponse à une demande d’assistance est conforme à la procédure et adaptée    |
| #. Traiter des demandes concernant les applications | à l’utilisateur.                                                                  |
|                                                     | - La méthode de diagnostic de résolution d’un incident est adéquate et efficiente.|
|                                                     | - Une solution à l’incident est trouvée et mise en œuvre.                         |
|                                                     | - Le cycle de résolution des demandes respecte les normes et standards du         |
|                                                     | prestataire informatique.                                                         |
|                                                     | - L’utilisation d’un logiciel de gestion de parc et d’incidents est maîtrisée.    |
|                                                     | - Le compte rendu d’intervention est clair et explicite.                          |
|                                                     | - La communication écrite et orale est adaptée à l’interlocuteur.                 |
+---+---+

## B1.3 : Développer la présence en ligne de l’organisation
+-----------------------------------------------------+-----------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                  |
+=====================================================+=============================================================================+
| #. Participer à la valorisation de l’image de       | - L’image de l’organisation est conforme aux attentes et valorisée.         |
| l’organisation sur les médias numériques en tenant  | - Les enjeux économiques liés à l’image de l’organisation sont identifiés   |
| compte du cadre juridique et des enjeux économiques | et les obligations juridiques sont respectées.                              |
| #. Référencer les services en ligne de              | - Les mentions légales sont accessibles et conformes à la législation.      |
| l’organisation et mesurer leur visibilité           | - La visibilité des services en ligne de l’organisation est satisfaisante.  |
| #. Participer à l’évolution d’un site Web exploitant| - Le site Web a évolué conformément au besoin exprimé.                      |
| les données de l’organisation                       |                                                                             |
+---+---+

## B1.4 : Travailler en mode projet
+-----------------------------------------------------+-----------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                  |
+=====================================================+=============================================================================+
| #. Analyser les objectifs et les modalités          | - Les objectifs et les modalités d’organisation du projet sont explicités.  |
| d’organisation d’un projet                          | - L’analyse des besoins et de l’existant est pertinente.                    |
| #. Planifier les activités                          | - Les activités personnelles sont planifiées selon une méthodologie donnée  |
| #. Évaluer les indicateurs de suivi d’un projet et  | et les ressources humaines, matérielles et logicielles nécessaires sont     |
| analyser les écarts                                 | mobilisées de manière efficace et pertinente.                               |
|                                                     | - Le découpage en tâches est réaliste.                                      |
|                                                     | - Les livrables sont conformes.                                             |
|                                                     | - Le projet est documenté.                                                  |
|                                                     | - Un compte rendu clair et concis est réalisé et les écarts sont justifiés. |
|                                                     | - La communication écrite et orale est adaptée à l’interlocuteur.           |
+---+---+

## B1.5 : Mettre à disposition des utilisateurs un service informatique
+-----------------------------------------------------+---------------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                      |
+=====================================================+=================================================================================+
| #. Réaliser les tests d’intégration et d’acceptation| - Des tests pertinents d’intégration et d’acceptation sont rédigés et effectués.|
| d’un service                                        | - Les outils de test sont utilisés de manière appropriée.                       |
| #. Déployer un service                              | - Un rapport de test du service est produit.                                    |
| #. Accompagner les utilisateurs dans la mise en     | - Un support d’information est disponible.                                      |
| place d’un service                                  | - Les modalités d’accompagnement sont définies.                                 |
|                                                     | - Le service déployé est opérationnel et donne satisfaction à l’utilisateur.    |
+---+---+

## B1.6 : Organiser son développement professionnel
+-----------------------------------------------------+--------------------------------------------------------------------------------+
| Compétences                                         | Indicateurs de performance                                                     |
+=====================================================+================================================================================+
| #. Mettre en place son environnement d’apprentissage| - Les besoins de formation sont identifiés pour assurer le support ou mettre à |
| personnel                                           | disposition un service.                                                        |
| #. Mettre en œuvre des outils et stratégies de      | - L’environnement d’apprentissage personnel est délimité et expliqué.          |
| veille informationnelle                             | - La veille est régulière et vise à :                                          |
| #. Gérer son identité professionnelle               |    - repérer les techniques et technologies émergentes du secteur informatique;|
| #. Développer son projet professionnel              |    - utiliser de manière approfondie des moyens de recherche d'information;    |
|                                                     |    - renforcer ses compétences.                                                |
|                                                     | - L’identité professionnelle est pertinente et visible sur un réseau social    |
|                                                     | professionnel.                                                                 |
+---+---+

# Bloc de compétences n°2 option B « Solutions logicielles et applications métiers » ‐ Conception et développement d’applications (E5)

## B2.1 : Concevoir et développer une solution applicative
* Analyser un besoin exprimé et son contexte juridique
* Participer à la conception de l’architecture d’une solution applicative
* Modéliser une solution applicative
* Exploiter les ressources du cadre applicatif (*framework*)
* Identifier, développer, utiliser ou adapter des composants logiciels
* Exploiter les technologies Web pour mettre en œuvre les échanges entre applications, y compris de mobilité
* Utiliser des composants d’accès aux données
* Intégrer en continu les versions d’une solution applicative
* Réaliser les tests nécessaires à la validation ou à la mise en production d’éléments adaptés ou développés
* Rédiger des documentations technique et d’utilisation d’une solution applicative
* Exploiter les fonctionnalités d’un environnement de développement et de tests

## B2.2 : Assurer la maintenance corrective ou évolutive d’une solution applicative
* Recueillir, analyser et mettre à jour les informations sur une version d’une solution applicative
* Évaluer la qualité d'une solution applicative
* Analyser et corriger un dysfonctionnement
* Mettre à jour des documentations technique et d’utilisation d’une solution applicative
* Élaborer et réaliser les tests des éléments mis à jour

## B2.3 : Gérer les données
* Exploiter des données à l’aide d’un langage de requêtes
* Développer des fonctionnalités applicatives au sein d’un système de gestion de base de données (relationnel ou non)
* Concevoir ou adapter une base de données
* Administrer et déployer une base de données


# Bloc de compétences n°3 ‐ Cybersécurité des services informatiques (E6)

## B3.1 : Protéger les données à caractère personnel
* Recenser les traitements sur les données à caractère personnel au sein de l’organisation
* Identifier les risques liés à la collecte, au traitement, au stockage et à la diffusion des données à caractère personnel
* Appliquer la réglementation en matière de collecte, de traitement et de conservation des données à caractère personnel
* Sensibiliser les utilisateurs à la protection des données à caractère personnel

## B3.2 Préserver l'identité numérique de l’organisation
* Protéger l’identité numérique d’une organisation
* Déployer les moyens appropriés de preuve électronique

## B3.3 Sécuriser les équipements et les usages des utilisateurs
* Informer les utilisateurs sur les risques associés à l’utilisation d’une ressource numérique et promouvoir les bons usages à adopter
* Identifier les menaces et mettre en œuvre les défenses appropriées
* Gérer les accès et les privilèges appropriés
* Vérifier l’efficacité de la protection

## B3.4 : Garantir la disponibilité, l’intégrité et la confidentialité des services informatiques et des données de l’organisation face à des cyberattaques
* Caractériser les risques liés à l’utilisation malveillante d’un service informatique
* Recenser les conséquences d’une perte de disponibilité, d’intégrité ou de confidentialité
* Identifier les obligations légales qui s’imposent en matière d’archivage et de protection des données de l’organisation
* Organiser la collecte et la conservation des preuves numériques
* Appliquer les procédures garantissant le respect des obligations légales

## B3.5 : Assurer la cybersécurité d’une solution applicative et de son développement
* Participer à la vérification des éléments contribuant à la qualité d’un développement informatique
* Prendre en compte la sécurité dans un projet de développement d’une solution applicative
* Mettre en œuvre et vérifier la conformité d’une solution applicative et de son développement à un référentiel, une norme ou un standard de sécurité
* Prévenir les attaques
* Analyser les connexions (logs)
* Analyser des incidents de sécurité, proposer et mettre en œuvre des contre‐mesures

[référentiel]: https://www.reseaucerta.org/sites/default/files/sio/BTS_ServicesInformatiquesOrganisations2019.pdf
